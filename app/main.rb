
class Digger
    def initialize args
        @args = args

        @LEFT = 0 
        @UP   = 1
        @RIGHT= 2
        @DOWN = 3
        @EARTH = 0
        @GRASS = 1
        @SKY = 2
        @HOLE = 3
        @COIN = 4
        @PLAYER = 5
        @ENEMY = 6
        @SPRITE_W = 32
        @SPRITE_H = 32
        @X_MAX = 20
        @Y_MAX = 15
        @colours = [
            [170, 85, 0],
            [65, 255, 0],
            [0, 125, 255],
            [0, 0, 0],
            [255, 255, 0],
            [255, 255, 255],
            [255, 0, 0],
        ]


        @tick_count = 0
        @speed = 20
        @num_coints = 0
        @dig_pos_x = 10
        @dig_pos_y = 6
        @enemy_pos_x = 0
        @enemy_pos_y = 5
        @enemy_state = 0
        @enemy_dir = @RIGHT
        @dig_dir = @DOWN

        @tiles = []
        @gfxtiles = []
        for row in 0..@Y_MAX-1
            @tiles[row] = []
            @gfxtiles[row] = []
            for col in 0..@X_MAX-1
                @tiles[row][col] = 0
                @gfxtiles[row][col] = 0
            end
        end
        init_tiles

        setup
    end


    def setup
        @dig_pos_x = 10
        @dig_pos_y = 6
        @dig_dir = @DOWN
        @enemy_pos_x = 0
        @enemy_pos_y = 5
        @enemy_state = 0
        @enemy_dir = @RIGHT

        fillPatterns = [
            [ 0, 5-1, @SKY ],
            [ 5, 6-1, @GRASS ],
            [  6, @Y_MAX-1, @EARTH ],
        ]

        for pattern in fillPatterns
            for Y in pattern[0]..pattern[1]
                for X in 0..@X_MAX-1
                    update_tile pattern[2], Y, X
                end
            end
        end

        update_tile @HOLE, @dig_pos_y, @dig_pos_x
        update_tile @SKY, @dig_pos_y-1, @dig_pos_x
        for i in 1..6
            y, x = rand(@Y_MAX-6)+6, rand(@X_MAX)
            if @tiles[y][x] != @COIN
                update_tile @COIN, y, x
                @num_coins += 1
            end
        end  
    end


    def draw_tile t, y, x
        r, g, b = @colours[t]
        @args.outputs.solids << {x:x, y:720-y, w:@SPRITE_W, h:@SPRITE_H, r:r, g:g, b:b}
    end

    def encode_tile t, y, x
        r, g, b = @colours[t]
        return {x:x, y:720-y, w:@SPRITE_W, h:@SPRITE_H, r:r, g:g, b:b}
    end

    def write_tile t, y, x
        @tiles[y][x] = t
        @gfxtiles[y][x] = encode_tile t, y*@SPRITE_H, x*@SPRITE_W
    end

    def init_tiles
        for Y in 0..@Y_MAX-1
            for X in 0..@X_MAX-1
                @gfxtiles[Y][X] = encode_tile @tiles[Y][X], Y*@SPRITE_H, X*@SPRITE_W
            end
        end
        @args.outputs.static_solids << @gfxtiles
    end

    def show_tile t, y, x
        r, g, b = @colours[t]
        @args.outputs.static_solids[y*@X_MAX+x].r = r 
        @args.outputs.static_solids[y*@X_MAX+x].g = g
        @args.outputs.static_solids[y*@X_MAX+x].b = b 
    end

    def update_tile t, y, x
        @tiles[y][x] = t
        show_tile t, y, x
    end

    def refresh_tile y, x
        show_tile @tiles[y][x], y, x
    end

    def clockwiseTurn dir
        return (dir + 1) % 4
    end

    def input
        @dig_dir = clockwiseTurn @dig_dir 
    end

    def move x, y, dir
        xoffset = [ -1, 0, 1, 0 ]  # LEFT, UP, RIGHT, DOWN
        yoffset = [  0, -1,0, 1 ]
        x = x + xoffset[dir]
        y = y + yoffset[dir]
        if x >= 0 and x < @X_MAX and y < @Y_MAX and y >= 6 then
            return x,y
        else
            return
        end
    end


    def updatePlayer
        newx,newy = move @dig_pos_x, @dig_pos_y, @dig_dir
        if newx then
            refresh_tile @dig_pos_y, @dig_pos_x
            @dig_pos_x = newx
            @dig_pos_y = newy
            if @tiles[@dig_pos_y][@dig_pos_x] == @COIN
                @num_coins -= 1
            end
            update_tile @HOLE, @dig_pos_y, @dig_pos_x
            show_tile @PLAYER, @dig_pos_y, @dig_pos_x
        end
    end

    def  updateEnemy count
        if count<0 then
            return
        end
   
        refresh_tile @enemy_pos_y, @enemy_pos_x 
        if @enemy_state == 0 then
            @enemy_pos_x = @enemy_pos_x + 1
            if @enemy_pos_x == 10 then
                @enemy_state = 1
                @enemy_dir = @DOWN
            end
        elsif @enemy_state == 1 then
            newx, newy = move @enemy_pos_x, @enemy_pos_y, @enemy_dir
            if newx and @tiles[newy][newx] == @HOLE then
                @enemy_pos_x = newx
                @enemy_pos_y = newy
            else
                @enemy_dir = clockwiseTurn @enemy_dir
                updateEnemy(count-1)
            end
        end

        show_tile @ENEMY, @enemy_pos_y, @enemy_pos_x 
    end


    def update
        coins = @num_coins

        if coins == 0 or ( @dig_pos_x == @enemy_pos_x and
                @dig_pos_y == @enemy_pos_y ) then
            #speed *= 0.75;
            setup()
        end

        @tick_count += 1
        if @tick_count > @speed then
            @tick_count = 0
            updatePlayer
            updateEnemy 1
        end
    end

    def tick
        #@args.outputs.sprites << [576, 280, 128, 101, 'dragonruby.png']
        if @args.inputs.keyboard.key_down.a or
           @args.inputs.controller_one.key_down.a then
         input
        end
        update
        @args.outputs.labels << [10, 230, "coins: #{@num_coins}"]
    end    

end


def tick args
    args.state.game ||= Digger.new args
    args.state.game.tick
    
    args.outputs.labels << [10, 210, "framerate: #{args.gtk.current_framerate.round}"]
end
